* Add event check-in functionality via QR codes (CWA)
* Create QR codes for your events from within the app (CWA)
* Add a warning card on the main screen when battery optimizations are enabled
* Fixed some more visual glitches on Android 5
* Reduce apk size by not prerendering all vector graphics

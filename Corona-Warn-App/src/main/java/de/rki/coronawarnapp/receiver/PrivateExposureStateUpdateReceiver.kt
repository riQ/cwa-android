package de.rki.coronawarnapp.receiver

/**
 * Non-exported version of [ExposureStateUpdateReceiver] that be called without permission by the embedded microG ENF.
 */
class PrivateExposureStateUpdateReceiver : ExposureStateUpdateReceiver() {
    companion object {
        private val TAG: String? = PrivateExposureStateUpdateReceiver::class.simpleName
    }
}
